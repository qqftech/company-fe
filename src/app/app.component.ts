import {Component} from '@angular/core';
import {TranslateService} from "@ngx-translate/core";
import {PrimeNGConfig} from "primeng/api";
import {Title} from "@angular/platform-browser";


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'company-fe'


  constructor(translate: TranslateService, private primengConfig: PrimeNGConfig, private titleService: Title) {
    this.titleService.setTitle("qqftech Company");


    this.primengConfig.ripple = true;

    translate.addLangs(['en', 'es']);
    // this language will be used as a fallback when a translation isn't found in the current language
    translate.setDefaultLang('es');
    translate.get('primeng').subscribe(res => {
      const translations = this.fixPrimeNGTranslations(res);
      this.primengConfig.setTranslation(translations);
    });

    // the lang to use, if the lang isn't available, it will use the current loader to get them
    let lang = localStorage.getItem('language');
    if (!lang) {
      lang = navigator.language;
    }
    switch (lang.split('-')[0]) {
      case 'es':
        translate.use('es');
        localStorage.setItem('language', 'es');
        break;
      case 'en':
      default:
        translate.use('en');
        localStorage.setItem('language', 'en');
        break;
    }
  }

  /**
   * Fix PrimeNG translations are not working correctly with the ngx-translate-messageformat-compiler plugin for NGX translate.
   * @param translations
   * @private
   */
  private fixPrimeNGTranslations(translations: any): any {
    switch (typeof translations) {
      case 'function':
        return translations();
      case 'object':
        if (Array.isArray(translations)) {
          return translations.map(t => this.fixPrimeNGTranslations(t));
        }

        for (const key in translations) {
          if (key in translations) {
            translations[key] = this.fixPrimeNGTranslations(translations[key]);
          }
        }
        return translations;
      default:
        return translations;
    }
  }


}
