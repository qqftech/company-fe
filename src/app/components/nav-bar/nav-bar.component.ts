import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {TranslateService} from "@ngx-translate/core";
import {LoginService} from "../../core/services/login.service";

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.scss']
})
export class NavBarComponent implements OnInit {
  selectedLanguage: string;
  selectedPage = 0;
  showNavBarMenu = false;

  constructor(private router: Router,
              private translate: TranslateService,
              private auth: LoginService) {
    this.selectedLanguage = localStorage.getItem('language') ?? 'es';
  }

  ngOnInit(): void {
    const selectedPage = localStorage.getItem('page');
    if (selectedPage) {
      this.selectedPage = Number(selectedPage);
    }
  }

  openLandingPage() {
    this.router.navigate(['/']);
  }

  openTechPage() {
    this.router.navigate(['/technology']);
  }

  openTeamPage() {
    this.router.navigate(['/team']);
  }

  openLoginPage() {
    this.router.navigate(['/login']);
  }

  openRegistrationPage() {
    this.router.navigate(['/register']);
  }

  openSecretPage() {
    this.router.navigate(['/evaluation']);
  }

  openDiscord() {
    this.auth.openDiscord();
  }

  disconnect() {
    this.auth.setUserValue(undefined);
    let lang = localStorage.getItem("language");
    localStorage.clear();
    if (lang)
      localStorage.setItem("language", lang);
    this.router.navigate(['/']); // landing page
  }

  changeLanguage() {
    let lang: string;
    if (this.selectedLanguage === "es")
      lang = "en";
    else
      lang = "es";
    this.translate.use(lang);
    localStorage.setItem('language', lang);
    this.selectedLanguage = lang;
  }

  isLogged() {
    return this.auth.isLoggedIn();
  }

  isLeader() {
    return true;
  }

  changeMenuNavBar() {
    this.showNavBarMenu = !this.showNavBarMenu;
  }
}
