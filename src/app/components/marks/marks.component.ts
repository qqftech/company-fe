import {Component, Input} from '@angular/core';
import {TranslateService} from "@ngx-translate/core";
import {Nota} from "../../core/models/nota";

@Component({
  selector: 'app-marks',
  templateUrl: './marks.component.html',
  styleUrls: ['./marks.component.scss']
})
export class MarksComponent {

  @Input() notas!: Nota[];
  @Input() title!: string;

  constructor(private translate: TranslateService) {
  }

}
