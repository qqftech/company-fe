import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class ApiService {
  readonly ENDPOINT = 'http://qqft-be.qqftech.live';
  readonly discordURL = 'https://discord.com/channels/860217673878601790/860217673878601793';

  constructor(private http: HttpClient) {
  }

  //hacemos una getRequest cogiendo el endpoint que le definimos
  get<T>(endpoint: string): Observable<T> {
    return this.http.get<T>(`${this.ENDPOINT}/${endpoint}`);
  }

  post<T>(endpoint: string, body: any): Observable<T> {
    return this.http.post<T>(`${this.ENDPOINT}/${endpoint}`, body);
  }

  tryConnection() {
    this.get("hm").subscribe((x) => console.log(x));
  }

}
