import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import {User} from "../models/user";
import {ApiService} from "./api.service";
import {AuthID} from "../models/authID";


@Injectable({
  providedIn: 'root',
})
export class LoginService {

  private token?: string;

  constructor(private api: ApiService) {
    const userToken = localStorage.getItem('token');
    if (userToken) {
      this.token = userToken;
    }
  }

  setUserValue(value?: string) {
    this.token = value;
    localStorage.setItem("token", <string>this.token);
  }

  //loginStatus, mando name password, recibo token
  getUserId(user: User): Observable<AuthID> {
    return this.api.get('login?' + 'matricula=' + user.noEnrollment + '&password=' + user.password);
  }

  isLoggedIn(): boolean {
    return this.token !== undefined;
  }

  openDiscord() {
    window.open(this.api.discordURL, "_blank");
  }
}

