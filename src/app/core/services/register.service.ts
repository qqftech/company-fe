import { Injectable } from '@angular/core';
import {Observable} from "rxjs";
import {AuthID} from "../models/authID";
import {ApiService} from "./api.service";
import {Register} from "../models/register";

@Injectable({
  providedIn: 'root'
})
export class RegisterService {

  constructor(private api: ApiService) { }

  //loginStatus, mando name password, recibo token
  setUser(user: Register): Observable<AuthID> {
    return this.api.post('register', user);
  }
}
