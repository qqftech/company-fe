import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiService } from './api.service';
import {Member} from "../models/member";

@Injectable({ providedIn: 'root' })
export class MemberService {
  constructor(private api: ApiService) {}

  getMember(id: string): Observable<Member> {
    return this.api.get(`teamMember/${id}`);
  }

  getMembers(): Observable<Member[]> {
    return this.api.get('teamMembers');
  }
}
