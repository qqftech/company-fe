export class Nota {
  value: number;
  info: string;

  constructor(nota: number, info: string) {
    this.value = nota;
    this.info = info;
  }
}
