export class Member {
  matricula?: string;
  nombre?: string;
  foto_url?: string;
  categoria?: 'executives' | 'developers';

  constructor(matricula: string, nombre: string, foto_url: string, categoria: "executives" | "developers") {
    this.matricula = matricula;
    this.nombre = nombre;
    this.foto_url = foto_url;
    this.categoria = categoria;
  }
}
