import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, ValidationErrors, Validators} from "@angular/forms";
import {TranslateService} from '@ngx-translate/core';
import {RegisterService} from '../../core/services/register.service';
import {Register} from "../../core/models/register";
import {Router} from "@angular/router";
import {LoginService} from "../../core/services/login.service";
import {HttpStatusCode} from "@angular/common/http";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  form!: FormGroup;
  loading = false;
  error = 0;
  // Password format checks
  containsLowercase = false;
  containsUppercase = false;
  containsNumber = false;
  containsMinLength = false;

  public RegExPass = '^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z]).{8,}$';
  // public emailUPM = '^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$'; // @alumnos.upm.es
  private RegExEmail = '^[a-zA-Z0-9_.+-]+@(?:(?:[a-zA-Z0-9-]+\\.)?[a-zA-Z]+\\.)?(alumnos.upm.es)$';


  constructor(
    private registerService: RegisterService,
    private loginService: LoginService,
    private translate: TranslateService,
    private router: Router
  ) {
  }

  ngOnInit(): void {
    // create the form
    this.form = new FormGroup({
        name: new FormControl(null, Validators.required),
        numberPlate: new FormControl(null, [Validators.required]),
        email: new FormControl(null, [Validators.required, Validators.pattern(this.RegExEmail)]),
        description: new FormControl(null, Validators.required),
        password: new FormControl(null, [Validators.required, Validators.pattern(this.RegExPass)]),
        passwordConfirmation: new FormControl(null, [Validators.required, Validators.pattern(this.RegExPass)]),
        terms: new FormControl(null, Validators.requiredTrue),
      }
    );
  }


  /**
   * Custom validator that compares the new password and its confirmation.
   *
   * @param frm The form group that will be validated.
   * @return null when passwords match, or a {mismatch: true} when passwords are not the same
   *
   */
  passwordMatchValidator(frm: FormGroup): ValidationErrors | null {
    const passValue = frm.controls.password?.value;
    const passConfirmValue = frm.controls.passwordConfirmation?.value;
    const passwordOk =
      (passValue && passConfirmValue) && (passValue === passConfirmValue) &&
      (passValue?.length >= 8) && this.hasNumber(passValue) &&
      this.hasLowerCase(passValue) && this.hasUpperCase(passValue);
    return passwordOk ? null : {mismatch: true};
  }

  passwordConditions(): ValidationErrors | null {
    return this.containsLowercase && this.containsMinLength
    && this.containsNumber && this.containsUppercase ? null : {mistmach: true};
  }

  /**
   * onPasswordChange
   * @description Check the password format (min length 8, at least one number, uppercase and lowercase)
   * @param frm The form group that will be validated.
   * @return void
   */
  onPasswordChange(frm: FormGroup) {
    this.containsMinLength = frm.controls.password?.value?.length >= 8;
    this.containsLowercase = this.hasLowerCase(frm.controls.password?.value);
    this.containsUppercase = this.hasUpperCase(frm.controls.password?.value);
    this.containsNumber = this.hasNumber(frm.controls.password?.value);
  }

  /**
   * hasNumber
   * @description Check if there is any number in a password string
   * @param password String password to check
   * @return boolean
   */
  hasNumber(password: string): boolean {
    return (/\d/.test(password));
  }

  /**
   * hasLowerCase
   * @description Check if there is any lowercase in a password string
   * @param password String password to check
   * @return boolean
   */
  hasLowerCase(password: string): boolean {
    return (/[a-z]/.test(password));
  }

  /**
   * hasUpperCase
   * @description Check if there is any uppercase in a password string
   * @param password String password to check
   * @return boolean
   */
  hasUpperCase(password: string): boolean {
    return (/[A-Z]/.test(password));
  }


  registerUser() {
    this.loading = true;
    let userRegistration = new Register(this.form.value.numberPlate, this.form.value.email, this.form.value.name, this.form.value.password);
    this.registerService.setUser(userRegistration).subscribe({
      next: authId => {
        console.log(authId.token);
        this.loginService.setUserValue(authId.token);
        this.loading = false;
        this.router.navigate(["/evaluation"]);
      },
      error: (err) => {
        if(err.status === HttpStatusCode.BadRequest){
          this.error = 1;
        } else{
          this.error = 2;
        }
        this.loading = false;
      }
    });
  }

}
