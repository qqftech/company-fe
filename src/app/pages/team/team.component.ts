import {Component, OnInit} from '@angular/core';
import {MemberService} from "../../core/services/member.service";
import {Member} from "../../core/models/member";

@Component({
  selector: 'app-team',
  templateUrl: './team.component.html',
  styleUrls: ['./team.component.scss']
})
export class TeamComponent implements OnInit {

  executives?: Member[];
  developers?: Member[];

  constructor(private memberService: MemberService) {
  }

  ngOnInit(): void {
    const findDevs = this.memberService.getMembers().subscribe(
      (response: Member[]) => {
        this.executives = this.createExecutives();
        this.developers = response.filter((m) => m.categoria === 'developers');
      },
      (error) => {
        this.executives = this.createExecutives();
        this.developers = this.createDevs();
      });

    // set a timer of 3s to the backend query, if not answer unsubscribe and create
    setTimeout(() => {
      findDevs.unsubscribe();
      this.executives = this.createExecutives();
      this.developers = this.createDevs();
    }, 3000);
  }

  createExecutives() {
    const sara = new Member("1", "Sara", "assets/img/FakeMembers/exec/1.jpg", 'executives');
    const eva = new Member("2", "Eva", "assets/img/FakeMembers/exec/2.jpg", 'executives');
    const sergio = new Member("3", "Sergio", "assets/img/FakeMembers/exec/3.jpg", 'executives');
    const oscar = new Member("4", "Oscar", "assets/img/FakeMembers/exec/4.jpg", 'executives');
    const angel = new Member("5", "Angel", "assets/img/FakeMembers/exec/5.jpg", 'executives');
    return [sara, eva, sergio, oscar, angel];
  }

  createDevs() {
    const dev = new Member("dev", "dev", "assets/img/FakeMembers/dev/1.jpg", 'executives');
    let devs: Member[] = [];
    for (let i = 0; i < 50; i++) {
      devs.push(dev);
    }
    return devs;
  }
}
