import {Component, OnInit} from '@angular/core';
import {LoginService} from "../../core/services/login.service";
import {User} from "../../core/models/user";
import {HttpStatusCode} from "@angular/common/http";
import {Router} from "@angular/router";


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  user = new User();
  errorMessage = 0;
  loading = false;
  token?: string;


  constructor(private loginService: LoginService, private router: Router) {
  }

  ngOnInit(): void {
  }

  login() {

    // mock connection with server
    this.loginService.setUserValue("nfrhogfshuiewy7835423tgsdy");
    this.router.navigate(['/evaluation']);

    this.loading = true;
    this.loginService.getUserId(this.user).subscribe({
      next: (authId) => {
        console.log(authId.token);
        this.loginService.setUserValue(authId.token);
        this.loading = false;
        this.errorMessage = 0;
        this.router.navigate(['/evaluation']);
      },
      error: (error) => {
        if (error.status === HttpStatusCode.BadRequest) {
          this.errorMessage = 2;
        } else {
          this.errorMessage = 1;
        }
        this.loading = false;
        console.log(error);
      }
    });
  }


}
