import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {LandingComponent} from "./pages/landing/landing.component";
import {NotfoundComponent} from "./pages/notfound/notfound.component";
import {LoginComponent} from "./pages/login/login.component";
import {RegisterComponent} from "./pages/register/register.component";
import {TechComponent} from "./pages/tech/tech.component";
import {TeamComponent} from "./pages/team/team.component";
import {EvaluationComponent} from "./pages/evaluation/evaluation.component";
import {AuthGuard} from "./core/guards/auth.guard";

export const routes: Routes = [
  {path: '', component: LandingComponent},
  {path: 'technology', component: TechComponent},
  {path: 'team', component: TeamComponent},
  {path: 'login', component: LoginComponent},
  {path: 'register', component: RegisterComponent},
  {path: 'evaluation', component: EvaluationComponent, canActivate: [AuthGuard]},
  {path: '**', component: NotfoundComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
