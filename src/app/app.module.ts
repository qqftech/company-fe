import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {NavBarComponent} from './components/nav-bar/nav-bar.component';
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {ConfirmDialogModule} from "primeng/confirmdialog";
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import {HttpClient, HttpClientModule} from '@angular/common/http';

import {LandingComponent} from './pages/landing/landing.component';
import {NotfoundComponent} from './pages/notfound/notfound.component';
import {LoginComponent} from './pages/login/login.component';
import {RegisterComponent} from './pages/register/register.component';
import {TechComponent} from './pages/tech/tech.component';
import {TeamComponent} from './pages/team/team.component';
import {EvaluationComponent} from './pages/evaluation/evaluation.component';
import {TeamMemberComponent} from "./components/team-member/team-member.component";
import {ProgressSpinnerModule} from "primeng/progressspinner";
import {FooterComponent} from './components/footer/footer.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {KeyFilterModule} from 'primeng/keyfilter';
import {PasswordModule} from 'primeng/password';
import {DividerModule} from 'primeng/divider';
import {CheckboxModule} from 'primeng/checkbox';
import {InputTextModule} from 'primeng/inputtext';
import {RippleModule} from 'primeng/ripple';
import {InputTextareaModule} from "primeng/inputtextarea";
import {ListboxModule} from "primeng/listbox";
import {TableModule} from "primeng/table";
import {DropdownModule} from "primeng/dropdown";
import {MarksComponent} from "./components/marks/marks.component";

@NgModule({
  declarations: [
    AppComponent,
    NavBarComponent,
    LandingComponent,
    NotfoundComponent,
    LoginComponent,
    RegisterComponent,
    TechComponent,
    TeamComponent,
    EvaluationComponent,
    TeamMemberComponent,
    FooterComponent,
    MarksComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    ConfirmDialogModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient],
      },
    }),
    ProgressSpinnerModule,
    FormsModule,
    ReactiveFormsModule,
    KeyFilterModule,
    PasswordModule,
    DividerModule,
    CheckboxModule,
    InputTextModule,
    RippleModule,
    InputTextareaModule,
    ListboxModule,
    TableModule,
    DropdownModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

export function HttpLoaderFactory(http: HttpClient): TranslateHttpLoader {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}
